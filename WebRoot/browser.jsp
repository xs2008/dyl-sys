<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page pageEncoding="UTF-8" isErrorPage="true"%>
<%@include file="/taglib.jsp"%>
<html>
<head>
<meta charset="UTF-8">
<meta name="renderer" content="webkit">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<title>Tbank浏览器兼容提示页</title>
</head>
<style>
html {
	height: 100%
}

.tbox_browser {
	background: url(../images/brower_bg.png) no-repeat center 100px;
	position: relative;
	height: 100%
}

.tbox_jihe {
	text-align: center;
	position: absolute;
	width: 100%;
	height: 410px;
	top: 50%;
	left: 0;
	margin-top: -205px
}

.tbox_jihe * {
	color: #666
}

.tbox_jihe_main {
	padding: 0 0 100px 0
}

.tbox_jihe_main label {
	font-size: 27px
}

.tbox_jihe p {
	font-size: 18px;
	margin: 0 0 5px 0
}

.tbox_jihe_ul {
	width: 300px;
	margin: 0 auto;
	margin-top: 100px
}

.tbox_jihe_ul a {
	float: left;
	margin: 0 20px;
	display: block
}

.tbox_jihe_ul a span {
	display: block
}
</style>
<body class="tbox_browser">
	<div class="tbox_jihe">
		<p>您的浏览器无法支持投票系统的各项功能，如果您使用的360等双核浏览器，</p>
		<p>请切换到极速模式浏览。您也可以升级到以下浏览器使用：</p>
		<div class="tbox_jihe_ul clearfix">
			<a
				href="https://www.baidu.com/baidu?tn=monline_3_dg&ie=utf-8&wd=chorme"
				target="_blank"> <img
				src="http://www.tranbanker.com/Public/Default/images/chorme.png" />
				<span>Chrome</span>
			</a> <a
				href="https://www.baidu.com/baidu?tn=monline_3_dg&ie=utf-8&wd=firfox"
				target="_blank"> <img
				src="http://www.tranbanker.com/Public/Default/images/firfox.png" /> <span>Firefox</span>
			</a>
		</div>
	</div>
</body>
</html>