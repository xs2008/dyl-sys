package dyl.sys.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import dyl.common.bean.R;
import dyl.common.util.Constants;
import dyl.common.util.Page;
import dyl.sys.bean.SysUser;
@Controller
public class BaseAction{
    @InitBinder("page")
    public void initBinderPage(WebDataBinder binder) {
      binder.setFieldDefaultPrefix("page.");   
    }
	protected Log log = LogFactory.getLog(getClass());
	public static String ADD="add";
	public static String EDIT="edit";
	private Page page = new Page();
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	public String getIP(HttpServletRequest request){  
	    if (request.getHeader("x-forwarded-for") == null) {  
	        return request.getRemoteAddr();  
	     }  
	    return request.getHeader("x-forwarded-for");  
	}
	public String getUsername(HttpServletRequest request){
		return ((SysUser)request.getSession().getAttribute(Constants.SESSION_USER_KEY)).getUsername();
	}
	public SysUser getSysUser(HttpServletRequest request){
		return  (SysUser)request.getSession().getAttribute(Constants.SESSION_USER_KEY);
	}
	public R returnByDbRet(int ret){
		if(ret>0){
			return new R(true);
		}else{
			return new R(false,"更新或者插入失败");
		}
	}
	public R returnByDbRet(int[] ret){
		for (int i = 0; i < ret.length; i++){
			if(ret[i]==0){
				return new R(false,"删除失败");
			}
		}
		return new R(true);
	}
	public R jsonSuccess(){
		return new R(true);
	}
	public R jsonSuccess(String msg){
		return new R(false,msg);
	}
	public R jsonFail(String msg){
		return new R(false,msg);
	}
	public R jsonFail(){
		return new R(false,"操作失败");
	}
}
